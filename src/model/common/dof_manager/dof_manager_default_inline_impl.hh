/**
 * @file   dof_manager_default_inline_impl.hh
 *
 * @author Nicolas Richart <nicolas.richart@epfl.ch>
 *
 * @date creation: Tue Aug 18 2015
 * @date last modification: Wed Mar 13 2019
 *
 * @brief  Implementation of the DOFManagerDefault inline functions
 *
 *
 * @section LICENSE
 *
 * Copyright (©) 2015-2021 EPFL (Ecole Polytechnique Fédérale de Lausanne)
 * Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 * Akantu is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * Akantu is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Akantu. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#include "dof_manager_default.hh"
/* -------------------------------------------------------------------------- */

#ifndef AKANTU_DOF_MANAGER_DEFAULT_INLINE_IMPL_HH_
#define AKANTU_DOF_MANAGER_DEFAULT_INLINE_IMPL_HH_

namespace akantu {} // namespace akantu

#endif /* __AKANTU_DOF_MANAGER_DEFAULT_INLINE_IMPL_HH_ */
